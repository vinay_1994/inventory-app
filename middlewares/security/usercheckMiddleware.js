'use strict';
const cryptoJs = require('crypto-js');
const dbCon = require('./../../utils/db');
const { asyncMiddleware } = require('../asyncMiddleware');
const httpUtil = require('../../utils/HttpUtil');
const ENCRYPTION_KEY = 'encryption key';

exports.encTokenCheck = asyncMiddleware(async (req, res, next) => {
	try {
		let encToken = req.headers['authorization'];
		if (encToken) {
			let token = encToken.split(' ')[1];
			let encryptedKey = cryptoJs.AES.decrypt(String(token), ENCRYPTION_KEY);
			let userId = encryptedKey.toString(cryptoJs.enc.Utf8);
			let user = await dbCon.query('SELECT * FROM tbl_users WHERE ID = ?', Number(userId));
			if (user.length === 1) {
				req.userDetails = user[0];
				delete req.userDetails.user_agent;
				next();
			} else {
				res.json(httpUtil.getUnauthorized());
			}
		} else {
			res.json(httpUtil.getUnauthorized());
		}
	} catch (error) {
		console.log(error);
		res.json(httpUtil.getUnauthorized());
	}
});
