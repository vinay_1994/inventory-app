'use strict';

module.exports = {
	ROOT_API: '/api/v1.0',
	AUTH: '/auth',
	DASHBOARD: '/dashboard',
	PERMISSION: '/getgenehmigung',
};
