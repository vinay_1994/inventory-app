'use strict';
const dbcon = require('./../../utils/db');
const QUERIES = require('./sql');
const httpUtil = require('./../../utils/HttpUtil');
const cryptoJS = require('crypto-js');
const moment = require('moment');
const asyncUtil = require('./../../middlewares/asyncMiddleware');
const ENCRYPTION_KEY = 'encryption key';

/* 
    signup /auth/signup route to create new user.
*/
exports.signup = async (req, res, next) => {
	try {
		let reqData = req.body;
		reqData.user_agent = JSON.stringify(req.useragent);
		let user = await dbcon.query(QUERIES.USER_CHECK, reqData.oAuth_user_id);
		if (user.length === 0) {
			reqData.register_date = moment().format('DD/MM/YYYY hh:mm:a');
			reqData.user_agent = JSON.stringify(req.useragent);
			await dbcon.query(QUERIES.ADD_USER, reqData);
			let addedUser = await dbcon.query(QUERIES.USER_CHECK, reqData.oAuth_user_id);
			let permissions = {
				user_id: addedUser[0].id,
				is_pro_user: 'no',
				is_pro_upload: 'no',
				is_data_sync: 'no',
				is_profile_edit: 'yes',
			};
			await dbcon.query(QUERIES.ADD_PERMISSION, permissions);
			let uid = cryptoJS.AES.encrypt(String(addedUser[0].id), ENCRYPTION_KEY).toString();
			res.json(httpUtil.getSuccess(uid));
		} else {
			let uid = cryptoJS.AES.encrypt(String(user[0].id), ENCRYPTION_KEY).toString();
			res.json(httpUtil.getSuccess(uid));
		}
	} catch (error) {
		console.log(error, 'from error');
		res.json(httpUtil.getException('Sign Up Error', null, error));
	}
};

exports.login = async (req, res, next) => {
	try {
		let id = cryptoJS.AES.decrypt(req.body.user_id, ENCRYPTION_KEY).toString(cryptoJS.enc.Utf8);
		let [err, data] = await asyncUtil.promiseHandler(dbcon.query(QUERIES.LOGIN_USER, id));
		if (!err) {
			if (data.length === 0) {
				res.json(httpUtil.getBadRequest('User Does Not Exist'));
			} else {
				let resData = {
					user_name: data[0].oAuthuser_name,
					profile_pic: data[0].oAuth_profile_pic,
				};
				res.json(httpUtil.getSuccess(resData, 'Login Success'));
			}
		} else {
			res.json(httpUtil.getException(null, 'login query error', data.errno));
		}
	} catch (error) {
		res.json(httpUtil.getException(error, 'Exception Caught', 5001));
	}
};
