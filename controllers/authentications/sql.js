'use strict';

module.exports = {
	USER_CHECK: 'SELECT * FROM tbl_users WHERE oAuth_user_id = ?',
	ADD_USER: 'INSERT INTO tbl_users SET ?',
	ADD_PERMISSION: 'INSERT INTO tbl_permission SET ?',
	LOGIN_USER: 'SELECT * FROM tbl_users WHERE ID = ?',
};
