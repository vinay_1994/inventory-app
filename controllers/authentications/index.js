'use strict';
const router = require('express').Router();
const { asyncMiddleware } = require('./../../middlewares/asyncMiddleware');
const { signup, login } = require('./handler');

//login handler
router.post('/signup', asyncMiddleware(signup));
router.post('/login', asyncMiddleware(login));

module.exports = router;
