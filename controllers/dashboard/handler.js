'use strict';
const dbcon = require('./../../utils/db');
const QUERIES = require('./sql');
const httpUtil = require('./../../utils/HttpUtil');
const cryptoJS = require('crypto-js');
const S3Util = require('./../../shared/uploader');
const { promiseHandler } = require('../../middlewares/asyncMiddleware');
const { query } = require('./../../utils/db');

/* 
    signup /auth/signup route to create new user.
*/
exports.getPermissions = async (req, res, next) => {
	try {
		if (req.userDetails.id) {
			let permission = [];
			let data = await dbcon.query(QUERIES.GET_PERMISSIONS, req.userDetails.id);
			Object.keys(data[0]).forEach((i) => {
				if (data[0][i] === 'yes') {
					permission.push(i);
				}
			});
			res.json(httpUtil.getSuccess(permission));
		} else {
			res.json(httpUtil.getBadRequest('permission id not found'));
		}
	} catch (error) {
		console.log(error);
		res.json(httpUtil.getException('Sign Up Error', null, error));
	}
};

exports.getItemList = async (req, res, next) => {
	try {
		if (req.userDetails.id) {
			let [err, data] = await promiseHandler(dbcon.query(QUERIES.GET_DASHBOARD_ITEMS, req.userDetails.id));
			if (!err) {
				res.json(httpUtil.getSuccess(data, 'Items Feteched'));
			} else {
				res.send(httpUtil.getException(data, 'feteching data failed'));
			}
		} else {
			res.json(httpUtil.getBadRequest('permission is not found'));
		}
	} catch (error) {
		res.json(httpUtil.getException());
	}
};

exports.uploaderDoc = async (req, res, next) => {
	const singleUpload = S3Util.upload.single('file');
	singleUpload(req, res, async function (err) {
		if (err) {
			res.status(500).send(err.message);
		} else {
			res.status(200).send(req.file);
		}
	});
};

exports.addItem = async (req, res, next) => {
	try {
		let itemList = {
			item_name: null,
			serial_number: null,
			purchase_date: null,
			warrenty_valid_till_date: null,
			price: null,
			price_um: null,
			item_brand_name: null,
			documents: null,
		};
		Object.keys(itemList).forEach((i) => {
			if (i === 'documents') {
				itemList.documents = JSON.stringify(req.body.doc);
			} else {
				itemList[i] = req.body[i];
			}
		});
		itemList.user_id = req.userDetails.id;
		let [err, data] = await promiseHandler(dbcon.query(QUERIES.ADD_ITEMS, itemList));
		if (!err) {
			res.json(httpUtil.getSuccess(null, 'Added Successfully'));
		} else {
			res.json(httpUtil.getException(data.sqlMessage, 'Add item failed'));
		}
	} catch (error) {
		console.log(error);
		res.json(httpUtil.getException(error, 'exception caught'));
	}
};

exports.deleteItem = async (req, res, next) => {
	try {
		let user_id = req.userDetails.id;
		let [err, data] = await promiseHandler(dbcon.query(QUERIES.DELETE_ITEMS, [user_id, req.params.itemId]));
		if (!err) {
			res.json(httpUtil.getSuccess(data, 'Deleted Successfully'));
		} else {
			res.json(httpUtil.getException(data.sqlMessage, 'Delete item failed'));
		}
	} catch (error) {
		console.log(error);
		res.json(httpUtil.getException(error, 'exception caught'));
	}
};
