'use strict';
const router = require('express').Router();
const multer = require('multer')();
const { asyncMiddleware } = require('./../../middlewares/asyncMiddleware');
const { getPermissions, getItemList, uploaderDoc, addItem, deleteItem } = require('./handler');
const API = require('./../../constants/Api');

//login handler
router.get(API.PERMISSION, asyncMiddleware(getPermissions));
router.get('/getList', asyncMiddleware(getItemList));
router.post('/upload', asyncMiddleware(uploaderDoc));
router.post('/add-item', asyncMiddleware(addItem));
router.delete('/delete-item/:itemId', asyncMiddleware(deleteItem));

module.exports = router;
