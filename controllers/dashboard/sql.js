'use strict';

module.exports = {
	GET_PERMISSIONS: 'SELECT * FROM tbl_permission WHERE user_id = ?',
	ADD_ITEMS: 'INSERT INTO tbl_item_details SET ?',
	GET_DASHBOARD_ITEMS: 'SELECT * FROM tbl_item_details  WHERE user_id = ?',
	DELETE_ITEMS: 'DELETE FROM tbl_item_details WHERE user_id = ? AND item_list_id = ?',
};
