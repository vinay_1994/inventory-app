'use strict';
const router = require('express').Router();
const API = require('./../constants/Api');
const authController = require('./authentications');
const dashboardController = require('./dashboard');
const { encTokenCheck } = require('./../middlewares/security/usercheckMiddleware');

router.use(API.AUTH, authController);
router.use(API.DASHBOARD, encTokenCheck, dashboardController);

module.exports = router;
