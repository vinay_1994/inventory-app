// Third Party Dependencies
const Joi = require('joi');

// Method to validate process-step
exports.processStepValidation = async data => {
  var schema = Joi.object().keys({
    idDivision: Joi.number().required(),
    idDepartment: Joi.number().required(),
    stepId: Joi.string()
      .max(50)
      .required(),
    stepName: Joi.string()
      .max(100)
      .required(),
    manufacturingScope: Joi.number().required(),
    processStepStatus: Joi.number().required(),
    createdBy: Joi.number().required(),
    modifiedBy: Joi.number().required(),
    idSupplier: Joi.number()
      .allow(null)
      .required(),
    stepDesc: Joi.string().allow('')
  });
  return await Joi.validate(data, schema);
};

// Update Process step Validation
exports.processStepUpdateValidation = async data => {
  var schema = Joi.object().keys({
    idDivision: Joi.number().required(),
    idDepartment: Joi.number().required(),
    stepId: Joi.string()
      .max(50)
      .required(),
    stepName: Joi.string()
      .max(100)
      .required(),
    manufacturingScope: Joi.number().required(),
    processStepStatus: Joi.number().required(),
    modifiedBy: Joi.number().required(),
    idSupplier: Joi.number()
      .allow(null)
      .required(),
    stepDesc: Joi.string().allow('')
  });
  return await Joi.validate(data, schema);
};

// Method to validate system attribute values
exports.systemAttributeValuesValidation = async data => {
  var schema = Joi.object().keys({
    system_attribute_code: Joi.string()
      .max(10)
      .required(),
    system_attribute_value: Joi.string()
      .max(100)
      .required(),
    id_system_attribute: Joi.number().required()
  });

  let services = Joi.array().items(schema);

  return await Joi.validate(data, services);
};

// Method Create  Process function  Validate
exports.processFunctionValidation = async data => {
  var schema = Joi.object().keys({
    divisionId: Joi.number().required(),
    departmentId: Joi.number().required(),
    functionId: Joi.string()
      .max(20)
      .required(),
    functionName: Joi.string()
      .max(50)
      .required(),
    processCode: Joi.number().required(),
    functionStatus: Joi.number().required(),
    createdBy: Joi.number().required()
  });

  return await Joi.validate(
    {
      divisionId: data.id_division,
      departmentId: data.id_department,
      functionId: data.function_id,
      functionName: data.function_name,
      processCode: data.id_process_code,
      functionStatus: data.function_status,
      createdBy: data.created_by
    },
    schema
  );
};

// Method Update  Process function  Validate
exports.processFunctionUpdateValidation = async data => {
  var schema = Joi.object().keys({
    divisionId: Joi.number().required(),
    departmentId: Joi.number().required(),
    functionId: Joi.string()
      .max(20)
      .required(),
    functionName: Joi.string()
      .max(50)
      .required(),
    processCode: Joi.number().required(),
    functionStatus: Joi.number().required(),
    modifiedBy: Joi.number().required()
  });

  return await Joi.validate(
    {
      divisionId: data.id_division,
      departmentId: data.id_department,
      functionId: data.function_id,
      functionName: data.function_name,
      processCode: data.id_process_code,
      functionStatus: data.function_status,
      modifiedBy: data.modified_by
    },
    schema
  );
};
// Method Create  Custom Attribute Value  Validate

exports.customAttributeValuesValidation = async data => {
  var schema = Joi.object().keys({
    custom_attribute_code: Joi.string()
      .max(10)
      .required(),
    custom_attribute_name: Joi.string()
      .max(50)
      .required(),
    id_custom_attribute: Joi.number().required()
  });
  let custom = Joi.array().items(schema);

  return await Joi.validate(data, custom);
};
