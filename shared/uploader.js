'use strict';
const AWS = require('aws-sdk');
const multer = require('multer');
const multerS3 = require('multer-s3');

const s3 = new AWS.S3({
	accessKeyId: 'AKIAYFDL2KYDRVTPYEQY',
	secretAccessKey: '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',
	region: 'ap-south-1',
});

const fileFilter = (req, file, cb) => {
	if (file.mimetype === 'audio/mpeg' || file.mimetype === 'video/mp4' || file.mimetype === 'image/jpeg') {
		cb(null, true);
	} else {
		cb(null, true);
		//cb(new Error('Invalid file type, only JPEG  MP4 And Mp3 is allowed!'), false);
	}
};

exports.upload = multer({
	fileFilter,
	storage: multerS3({
		acl: 'public-read',
		s3,
		bucket: function (req, file, cb) {
			cb(null, 'inventory.com');
		},
		metadata: function (req, file, cb) {
			cb(null, { fieldName: file.fieldname });
		},
		key: function (req, file, cb) {
			cb(null, req.userDetails.id + '/' + file.originalname);
		},
	}),
	limits: { fileSize: 4 * 1024 * 1024 },
});
